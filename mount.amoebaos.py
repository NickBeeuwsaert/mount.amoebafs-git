#!/usr/bin/env python
from fuse import Fuse
import fuse
import stat
import os
import os.path
import sys
import getpass
import errno
import json
import urllib2
import cookielib
import urllib
import tempfile
#maybe if uploads somehow get done, this will be used
# be the way that fuse works, implementing a API call to write x bytes from offset would be cool too
#import MultipartPostHandler
fuse.fuse_python_api = (0,2)
	

class FileStat(fuse.Stat):
    def __init__(self):
        self.st_mode = 0
        self.st_ino = 0
        self.st_dev = 0
        self.st_nlink = 0
        self.st_uid = 0
        self.st_gid = 0
        self.st_size = 0
        self.st_atime = 0
        self.st_mtime = 0
        self.st_ctime = 0
cj = cookielib.CookieJar()
opener = urllib2.build_opener(urllib2.HTTPCookieProcessor(cj))

def apiCall(ext, data, Server = "http://amoebaos.com/",API = "api/",headers= {"Content-Type":"application/x-www-form-urlencoded"},Opener = opener):
	#req = urllib2.Request(, data)
	#stream = urllib2.urlopen(req)
	stream = Opener.open(Server+API+ext, urllib.urlencode(data))
	return stream
	
def flag2mode(flags):
    md = {os.O_RDONLY: 'r', os.O_WRONLY: 'w', os.O_RDWR: 'w+'}
    m = md[flags & (os.O_RDONLY | os.O_WRONLY | os.O_RDWR)]

    if flags | os.O_APPEND:
        m = m.replace('w', 'a', 1)

    return m

class AmoebaFS(Fuse):
	def __init__(self, *args, **kw):
		Fuse.__init__(self, *args, **kw)

		loginInfo = {"ext":"login"}
		sys.stdout.write("Login: ");
		sys.stdout.flush()
		loginInfo["username"] = sys.stdin.readline().rstrip('\n')
		loginInfo["password"] = getpass.getpass("Password: ");
		stream = apiCall("login",loginInfo);
		result = stream.readline()
		stream.close()
		print result
	def readdir(self, path, offset):
		for r in '.','..':
			yield fuse.Direntry(r)
		readdirCall= apiCall("read_dir",{"dir":path})
		dirListing = json.loads(readdirCall.readline());
		readdirCall.close()
		for r in dirListing:
			yield fuse.Direntry(r["name"].encode("utf-8"))
	def read(self, path, length, offset):
		print "READ"
		file = apiCall("readfile",{"file":path});
		fileContent = file.read()
		file.close()
		return fileContent[offset:offset+length] 
	def open(self, path, flags):
		print "OPEN"
	def create(self, path, flags, mode):
		fileWrite = apiCall("writefile",{"data":"", "file":path,"overwrite":"false"});
		fileWrite.read()
		fileWrite.close()
	def write(self, path, buf,offset):
		print "WRITE", buf
		writeCall = apiCall("readfile",{"file":path})
		file = writeCall.read()
		writeCall.close()
		newFile = file[:offset] + buf + file[offset+len(buf):]
		fileWrite = apiCall("writefile",{"data":newFile, "file":path,"overwrite":"true"})
		fileWrite.read()
		fileWrite.close()
		return len(buf)
	def getattr(self, path):
		st = FileStat()
		if path == '/':
			st.st_mode = stat.S_IFDIR | 0755
			st.st_nlink = 2
		else:
			attrCall = apiCall("file_info",{"file":path})
			fileInfo = json.loads(attrCall.readline());
			attrCall.close()
			if fileInfo["result"] == True:
				if fileInfo["type"] == 'dir':
					st.st_mode = stat.S_IFDIR | 0755
					st.st_nlink = 2
				else: #if "type" in fileInfo:
					st.st_mode = stat.S_IFREG | 0777
					st.st_nlink = 1
					st.st_size = fileInfo["size"]
				if fileInfo["owner"] == "user":
					st.st_uid = os.getuid()
					st.st_gid = os.getgid()
					st.st_ctime = fileInfo["created"]
					st.st_mtime = fileInfo["modified"]
			if fileInfo["result"] == False:
				return -errno.ENOENT
		return st
	def rename(self, old, new):
		print "RENAME", old, new
		tail, head = os.path.split(new)
		renameOp = apiCall("renamefile",{"file":old,"newfile":new})
		result = json.loads(renameOp.readline());
		renameOp.close()
		if result["result"] == False: # Maybe add som better error checking here
			print result["message"]
			return -errno.EEXIST
	def unlink(self, path):
		print "UNLINK"
		unlinkCall = apiCall("deletefile",{"file":path})
		unlinkCall.read()
		unlinkCall.close()
	def truncate(self, path, len):
		pass

	def main(self, *a, **kw):
		#self.file_class = AmoebaFile
		return Fuse.main(self, *a, **kw)
def main():
	'''dirListing = json.loads(apiCall("read_dir", {"dir":"/"}).readline());
	for r in dirListing:
		print(r["name"])'''
	fs = AmoebaFS()
	#fs.setOpener(opener)
	fs.flags=0
	fs.multithreaded = 0
	fs.parse()
	fs.main()
if __name__ == "__main__":
	main()
